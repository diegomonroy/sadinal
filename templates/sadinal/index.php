<?php

/* ************************************************************************************************************************

SADINAL

File:			index.php
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2016

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

JHtml::_( 'behavior.framework', true );

// Variables

$site_base = $_SERVER['HTTP_HOST']; // e.g. www.amapolazul.com
$site_path = 'http://' . $site_base; // e.g. http://www.amapolazul.com
$app = JFactory::getApplication();
$option = JRequest::getVar( 'option' );
$view = JRequest::getVar( 'view' );
$Itemid = JRequest::getVar( 'Itemid' );
$pageclass = $app->getParams( 'com_content' );
$class = $pageclass->get( 'pageclass_sfx' );

// Template path

$path = 'templates/' . $this->template . '/';

// Modules

$show_banner = $this->countModules( 'banner' );
$show_home = $this->countModules( 'home' );
$show_left = $this->countModules( 'left' );

$style = 'small-12';

if ( $show_left ) {
	$style = 'small-9';
}

// Params

?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="<?php echo $this->language; ?>"><![endif]-->
<html class="no-js" lang="<?php echo $this->language; ?>">
	<head>
		<jdoc:include type="head" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="-va0MHyeEobIVMS7DMRJeFp-2YBv7RlXoLXJAtd8V7U">
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo $site_path; ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php echo $app->getCfg( 'sitename' ); ?>">
		<meta property="og:description" content="<?php echo $app->getCfg( 'MetaDesc' ); ?>">
		<meta property="og:image" content="<?php echo $site_path; ?>/<?php echo $path; ?>images/logo_ogp.png">
		<link rel="image_src" href="<?php echo $site_path; ?>/<?php echo $path; ?>images/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<link href="<?php echo $path; ?>js/foundation/css/normalize.css" rel="stylesheet">
		<link href="<?php echo $path; ?>js/foundation/css/foundation.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>js/jquery/fancybox/jquery.fancybox.css?v=2.1.5" rel="stylesheet" type="text/css" media="screen">
		<link href="<?php echo $path; ?>css/template.css" rel="stylesheet" type="text/css">
		<script src="<?php echo $path; ?>js/foundation/js/vendor/modernizr.js"></script>
		<!-- Begin Google Analytics -->
		<script>

			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-74928858-1', 'auto');
			ga('send', 'pageview');

		</script>
		<!-- End Google Analytics -->
	</head>
	<body>
		<!-- Begin Top -->
			<div class="top">
				<jdoc:include type="modules" name="top" style="xhtml" />
				<div class="clear"></div>
			</div>
		<!-- End Top -->
		<?php if ( $show_banner ) : ?>
		<!-- Begin Banner -->
			<div class="banner">
				<jdoc:include type="modules" name="banner" style="xhtml" />
				<div class="clear"></div>
			</div>
		<!-- End Banner -->
		<?php endif; ?>
		<!-- Begin Component -->
			<div class="component">
				<div class="row">
					<?php if ( $show_left ) : ?>
					<div class="small-3 columns">
						<jdoc:include type="modules" name="left" style="xhtml" />
					</div>
					<?php endif; ?>
					<div class="<?php echo $style; ?> columns">
						<jdoc:include type="component" />
					</div>
				</div>
			</div>
		<!-- End Component -->
		<?php if ( $show_home ) : ?>
		<!-- Begin Home -->
			<div class="home">
				<jdoc:include type="modules" name="home" style="xhtml" />
			</div>
		<!-- End Home -->
		<?php endif; ?>
		<!-- Begin Bottom -->
			<div class="bottom">
				<div class="row">
					<jdoc:include type="modules" name="bottom" style="xhtml" />
				</div>
			</div>
		<!-- End Bottom -->
		<!-- Begin Copyright -->
			<div class="copyright_wrap">
				<div class="copyright">
					&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo $this->baseurl; ?>"><?php echo $app->getCfg( 'sitename' ); ?></a>. Todos los derechos reservados. Sitio desarrollado por <a href="http://www.amapolazul.com" target="_blank">Amapolazul</a>.
				</div>
			</div>
		<!-- End Copyright -->
		<!-- Begin Main Scripts -->
			<script src="<?php echo $path; ?>js/foundation/js/vendor/jquery.js"></script>
			<script src="<?php echo $path; ?>js/foundation/js/foundation.min.js"></script>
			<script src="<?php echo $path; ?>js/jquery/fancybox/jquery.fancybox.pack.js?v=2.1.5" type="text/javascript"></script>
			<script>
				$(document).foundation();
			</script>
			<script src="<?php echo $path; ?>js/jquery/scripts.js" type="text/javascript"></script>
		<!-- End Main Scripts -->
	</body>
</html>